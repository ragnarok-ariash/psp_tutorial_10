package com.example;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTest {
	@Autowired
	StudentService service;

	@Test
	public void testSelectAllStudents() {
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 3, students.size());
	}

	@Test
	public void testSelectStudent() {
		StudentModel student = service.selectStudent("12345");
		Assert.assertNotNull("Gagal - student menghasilkan null", student);
		Assert.assertEquals("Gagal - nama student tidak sesuai database", "aName", student.getName());
		Assert.assertEquals("Gagal - GPA student tidak sesuai database", 3.45, student.getGpa(), 0.0);
	}

	@Test
	public void testCreateStudent() {
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		// Masukkan ke service
		service.addStudent(student);
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent() {
		String newName = "dName";
		StudentModel student = service.selectStudent("12345");
		Assert.assertNotNull("Gagal - student menghasilkan null", student);
		student.setName(newName);
		service.updateStudent(student);
		student = service.selectStudent("12345");
		// Cek apakah nama student berhasil di-update
		Assert.assertEquals("Gagal - nama student tidak berhasil diubah", newName, student.getName());	
	}
	
	@Test
	public void testDeleteStudent() {
		StudentModel student = service.selectStudent("12345");
		Assert.assertNotNull("Gagal - student menghasilkan null", student);
		service.deleteStudent(student.getNpm());
		student = service.selectStudent("12345");
		// Cek apakah student berhasil di-delete
		Assert.assertNull("Gagal - student tidak berhasil dihapus", student);	
	}
}
