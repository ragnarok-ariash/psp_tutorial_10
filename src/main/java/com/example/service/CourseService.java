package com.example.service;

import java.util.List;

import com.example.model.CourseModel;

public interface CourseService {
	CourseModel selectCourse(String courseId);

	List<CourseModel> selectAllCourses();

	void addCourse(CourseModel newCourse);

	void deleteCourse(String courseId);

	void updateCourse(CourseModel courseModel);

}
