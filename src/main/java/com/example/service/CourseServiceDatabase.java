package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.CourseMapper;
import com.example.model.CourseModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CourseServiceDatabase implements CourseService{
	@Autowired
    private CourseMapper courseMapper;

	@Override
	public CourseModel selectCourse(String courseId) {
		log.info ("select course with course id {}", courseId);
        return courseMapper.selectCourse(courseId);
	}

	@Override
	public List<CourseModel> selectAllCourses() {
		log.info ("select all courses");
        return courseMapper.selectAllCourses ();
	}

	@Override
	public void addCourse(CourseModel newCourse) {
		courseMapper.addCourse (newCourse);
	}

	@Override
	public void deleteCourse(String courseId) {
		courseMapper.deleteCourse(courseId);
    	log.info ("course " + courseId + " deleted");	
	}

	@Override
	public void updateCourse(CourseModel courseModel) {
		courseMapper.updateCourse(courseModel);
		log.info ("update course with id_course "+courseModel.getIdCourse()
		+" to name: "+courseModel.getNama()
		+" and sks: "+courseModel.getSks());
	}

}
